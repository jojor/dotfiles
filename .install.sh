#!/bin/bash

YUM_CMD=$(which yum)
APT_GET_CMD=$(which apt-get)

install_package() {
  if [[ ! -z $YUM_CMD ]]; then
     yum -y install $1
  elif [[ ! -z $APT_GET_CMD ]]; then
     apt-get -y install $1
  else
     echo "error can't install package $PACKAGE"
     exit 1;
  fi
}

echo "Installing git"
install_package git
echo "Installing vim"
install_package vim

echo "Installing zsh"
install_package zsh

echo "Installing oh my zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

echo "Install powerlever10k"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

echo "set zsh as default shell"
chsh -s "$(which zsh)"

echo "Recovering dotfiles"
git clone --recursive https://jojor@bitbucket.org/jojor/dotfiles.git ~/dotfiles
~/dotfiles/.setup.sh

echo "Setup done"

