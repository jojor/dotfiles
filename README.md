# My dotfiles and settings
Here I'll store my dotfiles for reference and to get an easy install on new
machines.
Also to keep them synchonized.

## Install
```
bash -c "$(curl -fsSL https://bitbucket.org/jojor/dotfiles/raw/master/.install.sh)"
```
