#!/bin/bash

dir=~/dotfiles
files=$(ls "$(dirname "$0")")

echo "Update dotfiles repository"
cd $(dirname "$0")
git pull -q

cd $dir

for file in $files
do
	echo "Delete old .$file"
	rm -rf ~/.$file
	echo "creating symlink"
	ln -s "$dir/$file" ~/."$file"
done

echo "Source zsh"
source ~/.zsh


echo "Done"
